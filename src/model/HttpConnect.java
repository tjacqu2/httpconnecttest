package model;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class HttpConnect {
	public void sendGetRequest()  throws Exception {
	    String urlString = "http://ptsv2.com/t/hdcur-1536336079/post";
	    String urlPlatString;
	    String urlModeString;
	    
	    URL url = new URL(urlString);
	    HttpURLConnection con = (HttpURLConnection) url.openConnection();
	    
	    BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
	    String fromServer;
	    StringBuffer response = new StringBuffer();
	    
	    while((fromServer = in.readLine()) != null) {
	      response.append(fromServer);
	    }
	    
	    in.close();
	    JSONParser parser = new JSONParser();
	    Object obj = parser.parse(response.toString());
	    JSONObject jObj = (JSONObject) obj;
	    String city = (String) jObj.get("City");
	    String color = (String) jObj.get("Color");
	    JSONArray nameList = (JSONArray) jObj.get("NameList");
	    System.out.println(city);
	    System.out.println(color);
	    @SuppressWarnings("unchecked")	    
		Iterator<String> iterator = nameList.iterator();
	    while(iterator.hasNext()) {
	    	System.out.println(iterator.next());
	    }
	    System.out.println(response);
	}
} 